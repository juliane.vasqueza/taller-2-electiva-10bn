package com.ecci.taller2

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_registry.*
import java.util.*

@Suppress("NAME_SHADOWING")
class RegistryActivity : AppCompatActivity() {

    //Arreglo para los check del MultiChoiceDialog
    private val hobbiesCheckedArray  = booleanArrayOf(false, false, false, false, false, false)

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registry)

        //Validar campos obligatorios
        buttonSubmit.setOnClickListener {
            validation()
        }

        //Ir Dejar vacios todos los campos de Formulario de registro
         buttonReset.setOnClickListener {
             resetEditText()
         }

        // Mostrar dialogo para seleccionar el tipo de documento
        editTextDocType.setOnClickListener{
            showDocTypeDialog()
        }

        //Mostrar dialogo para seleccionar la fecha de nacimiento
        editTextBirthday.setOnClickListener {
            showBirthdayDialog()
        }

        //Mostrar dialogo para seleccionar los hobbies
        editTextHobbies.setOnClickListener {
            showHobbiesDialog()
        }
    }

    //Mostrar dialogo para selccionar el tipo de documento
    private fun showDocTypeDialog(){
        //Se crea un objeto de tipo data para traer los tipos de documentos
        val data = Data("")
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.select_your_doc_type)
        builder.setItems(arrayOf(
                data.cc,
                data.ti,
                data.passport,
                data.ce,
                data.pep))
        {_, position ->
            //Se valida que tipo de documento fue seleccionado
            when(position) {
                0 -> editTextDocType.setText(data.cc)
                1 -> editTextDocType.setText(data.ti)
                2 -> editTextDocType.setText(data.passport)
                3 -> editTextDocType.setText(data.ce)
                4 -> editTextDocType.setText(data.pep)
            }
        }
        builder.show()
    }

    //Mostrar el dialogo para seleccionar la fecha de nacimiento
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.N)
    private fun showBirthdayDialog(){
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val dialog = DatePickerDialog(this, {
            _, year, month, day ->
            editTextBirthday.setText(
                    day.toString() + "/" + (month+1).toString() + "/"+year.toString())
        },year, month,day)
        dialog.setTitle(R.string.select_your_bithday)
        dialog.show()
    }

    private fun showHobbiesDialog(){
        //Se crea un objeto de tipo data para traer los tipos de documentos
        val data = Data("")
        //Arreglo de Hobbies
        val hobbiesArray = arrayOf(data.read, data.exercise, data.travel,
            data.sleep, data.watchTv, data.useCellPhone)
        //Se crea el Alert Dialog y se modifican los atributos
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.select_your_hobbies)
        builder.setMultiChoiceItems(hobbiesArray, hobbiesCheckedArray)
        {_, position, isChecked ->
            //Se almacenan los hobbies selecionados y los que no en el array
            hobbiesCheckedArray[position] = isChecked

        }
        builder.setPositiveButton(R.string.acept) { _, _ ->
            editTextHobbies.setText("")
            // se recorre el se agregan los hobbies cheked al textViewHobies
            // ademas se envia la info al activity mediante una función calback
            for (i in hobbiesCheckedArray.indices) {
                val checked =hobbiesCheckedArray[i]
                if (checked) {
                    editTextHobbies.setText(editTextHobbies.text.toString().plus(hobbiesArray[i].plus(", ")))
                }
            }
        }
        builder.setNegativeButton(R.string.cancel) { _, _ ->

        }
        builder.show()
    }

    private fun resetEditText(){
        //Se resetean las editText
        editTextName.setText("")
        editTextLastName.setText("")
        editTextDocType.setText("")
        editTextNumDoc.setText("")
        editTextBirthday.setText("")
        editTextHobbies.setText("")
        editTextPassword.setText("")
        editTextConfirmPassword.setText("")
        //Se resetea el arreglo de check para los hobbies
        for (i in hobbiesCheckedArray.indices){
            hobbiesCheckedArray[i] = false
        }
    }

    private fun validation(){
        // Si algun campo esta vacio, mostrar alertDialog con error
        if (editTextName.text.isEmpty() || editTextLastName.text.isEmpty() || editTextDocType.text.isEmpty() || editTextNumDoc.text.isEmpty()
                || editTextBirthday.text.isEmpty() || editTextHobbies.text.isEmpty() || editTextPassword.text.isEmpty() || editTextConfirmPassword.text.isEmpty()){
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.error)
            builder.setMessage(R.string.all_files_are_required)
            builder.setPositiveButton(R.string.acept){_,_ ->
            }
            builder.show()
         // Validar si las contraseñas coincidan
        }else if(!editTextPassword.text.toString().equals(editTextConfirmPassword.text.toString())){
            val builder = AlertDialog.Builder(this)
            builder.setTitle(R.string.error)
            builder.setMessage(R.string.no_match_passwords)
            builder.setPositiveButton(R.string.acept){_,_ ->
            }
            builder.show()
        }
        // en Caso de hacer diligenciado todos los campos y que las contraseñas coincidan
        else{
            showTermsConditionsDialog()

        }
    }

    private fun showTermsConditionsDialog(){
        val intent = Intent(this, HomeActivity::class.java)
        // Se agregan los extra con los datos del usuario
        intent.putExtra(R.string.name_key.toString(), editTextName.text.toString())
        intent.putExtra(R.string.lastName_key.toString(), editTextLastName.text.toString())
        intent.putExtra(R.string.docType_key.toString(), editTextDocType.text.toString())
        intent.putExtra(R.string.numDoc_key.toString(), editTextNumDoc.text.toString())
        intent.putExtra(R.string.birthday_key.toString(), editTextBirthday.text.toString())
        intent.putExtra(R.string.hobbies_key.toString(), editTextHobbies.text.toString())
        intent.putExtra(R.string.hobbiesCheckedArray_key.toString(), hobbiesCheckedArray)
        intent.putExtra(R.string.isActivity_key.toString(),true)
        val builder = TermsConditionsDialog(this, intent)
        builder.show()
    }
}