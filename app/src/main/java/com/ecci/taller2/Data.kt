package com.ecci.taller2

data class Data(var hi: String) {
    //Tipo de documentos
    val cc = "Cédula de Ciudadania"
    val ti = "Tarjeta de Identidad"
    val passport = "Pasaporte"
    val ce = "Cedula Extrangera"
    val pep = "Permiso Especial de Permanencia"

    //Hobies
    val read = "Leer"
    val exercise = "Hacer Deporte"
    val travel = "Viajar"
    val sleep = "Dormir"
    val watchTv = "Ver televisión"
    val useCellPhone ="Usar el celular"
}